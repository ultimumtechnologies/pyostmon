FROM nexus3.kb.cz:18443/kolla/debian-binary-cron:stein

ENV HOME /root
ENV OPENRC_FILE "/etc/kolla/admin-openrc.sh"
ENV RSYSLOG_SERVER "syslog-ha.sos.kb.cz"
ENV RSYSLOG_PORT "5171"
ENV RSYSLOG_FACILITY "local1.info"
ENV CRON_CHECK_COMPUTE_SERVICES="* * * * *"
ENV CRON_CHECK_ENDPOINTS="* * * * *"
ENV CRON_CHECK_AGENTS="*/5 * * * *"
ENV CRON_CHECK_QUOTAS="0 * * * *"
ENV QUOTAS_JSON_FILE "/etc/kolla/pyostmon-quotas.json"

WORKDIR /root

USER root

RUN export http_proxy=http://172.16.142.162:5865; apt-get update; apt-get upgrade;  apt-get install -y python-openstackclient
#RUN apt-get update; apt-get install -y python-openstackclient vim

COPY entrypoint /usr/bin/entrypoint
COPY pyostmon.py /usr/bin/pyostmon.py

VOLUME /etc/kolla

ENTRYPOINT ["entrypoint"]

