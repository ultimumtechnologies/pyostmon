#!/usr/bin/python

import sys
import argparse
import os
import datetime
from logging.handlers import SysLogHandler
import logging
import socket
import openstack
import requests
import json


CRITICAL = 50
FATAL = CRITICAL
ERROR = 40
WARNING = 30
WARN = WARNING
INFO = 20
DEBUG = 10
NOTSET = 0

_levelToName = {
    CRITICAL: 'CRITICAL',
    ERROR: 'ERROR',
    WARNING: 'WARNING',
    INFO: 'INFO',
    DEBUG: 'DEBUG',
    NOTSET: 'NOTSET',
}
_nameToLevel = {
    'CRITICAL': CRITICAL,
    'FATAL': FATAL,
    'ERROR': ERROR,
    'WARN': WARNING,
    'WARNING': WARNING,
    'INFO': INFO,
    'DEBUG': DEBUG,
    'NOTSET': NOTSET,
}
# TODO: socket.gaierror: [Errno -2] Name or service not known
# TODO: mapping RSYSLOG_LEVEL

class PyOSTMon(object):

    def __init__(self):
        self.envvars = os.environ
        self.endpoints = []

        # facility=17 -> LOG_LOCAL1
        fmt = logging.Formatter("1 %(asctime)s {} %(name)s - - "
                                "[msgInfo severity=\"%(levelname)s\" hostname=\"{}\" app=\"%(name)s\"] "
                                "%(levelname)s: %(message)s".format(self.envvars['HOSTNAME'], self.envvars['HOSTNAME']),
                                datetime.datetime.utcnow().isoformat())
        sysloghandler = SysLogHandler(facility=17,
                                      address=(self.envvars['RSYSLOG_SERVER'], self.envvars['RSYSLOG_PORT']),
                                      socktype=socket.SOCK_STREAM)
        sysloghandler.setFormatter(fmt)

        # streamhandler = logging.StreamHandler()
        # streamhandler.setFormatter(fmt)

        self.logger = logging.getLogger('PyOSTMon')
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(sysloghandler)
        # self.logger.addHandler(streamhandler)
        ## openstack.enable_logging(debug=True, path='openstack.log', stream=sys.stdout)

        self.conn = openstack.connect(verify=False, timeout=5)
        try:
            self.endpoints = [ep for ep in self.conn.identity.endpoints()]
        except:
            self.logger.critical('Openstack API (identity endpoint) is not accessible.')
            exit(1)

    def _level_to_num(self, str):
        return

    def check_endpoints(self):
        services = {s.id: {'desc': s.description, 'type': s.type} for s in self.conn.identity.services()}

        for endpoint in self.endpoints:
            try:
                r = requests.get(endpoint.url, timeout=10, allow_redirects=False, verify=False)
            except requests.exceptions.ConnectionError:
                self.logger.critical(
                    '{} {} endpoint {} is not accessible.'.format(services[endpoint.service_id]['desc'],
                                                                  endpoint.interface, endpoint.url))

    def check_agents(self):
        dead_agents = [agent for agent in self.conn.network.agents() if agent.is_admin_state_up and not agent.is_alive]
        for agent in dead_agents:
            self.logger.error('{} (id: {}) on host {} is down.'.format(agent.agent_type, agent.id, agent.host))

    def check_compute_services(self):
        dead_compute_services = [cservice for cservice in self.conn.compute.services()
                                 if cservice.status == 'enabled' and cservice.state != 'up']
        for cservice in dead_compute_services:
            self.logger.critical('Service {} on host {} is down.'.format(cservice.binary, cservice.host))

    def check_quotas(self):
        try:
            with open(self.envvars['QUOTAS_JSON_FILE']) as json_file:
                quotas_json = json.load(json_file)
        except IOError:
            self.logger.error('Cannot load quotas threshold definition file')
            exit(1)
        except KeyError:
            self.logger.error('Environment variable \'QUOTAS_JSON_FILE\' is not set.')
            exit(1)

        # watched metric with threshold
        # with open('/etc/kolla/test-data.json') as json_file:
        #     test_json = json.load(json_file)
        # monitor_metrics = {
        #       "ram": {
        #         "WARNING": {"percent":  50},
        #         "INFO": {"percent":  50},
        #         "CRITICAL": {"units_left": 100000}
        #       },
        #       "instances": {
        #         "WARNING": {"units_left":  1}
        #       },
        #       "cores": {
        #         "WARNING": {"percent":  90}
        #       },
        #       "volumes": {
        #         "WARNING": {"units_left":  5}
        #       },
        #       "gigabytes": {
        #         "WARNING": {"percent":  40},
        #         "CRITICAL": {"percent": 40}
        #       },
        #       "port": {
        #         "WARNING": {"percent":  95}
        #       }
        #     }

        ## sorted_monitor_metrics = {m: [{_nameToLevel[i]: v[i]} for i in v] for m, v in monitor_metrics.items()}

        ## Offline testing
        # from pprint import pprint
        #
        # current_metrics = test_json
        #
        # for mname, mdef in monitor_metrics.items():
        #
        #     value = current_metrics[mname]
        #     keyname = 'used' if 'used' in value else 'in_use'
        #
        #     curstate = dict()
        #     curstate['percent'] = round((float(value[keyname]) / float(value['limit'])) * 100, 1)
        #     curstate['units_left'] = int(value['limit']) - (value[keyname])
        #
        #     sorted_mdef_severities = sorted([_nameToLevel[i] for i in mdef.keys()], reverse=True)
        #     log_sent = False
        #
        #     for severity in sorted_mdef_severities:
        #         for unit in monitor_metrics[mname][_levelToName[severity]]:
        #             limit = mdef[_levelToName[severity]].get(unit)
        #             if limit:
        #                 if unit == 'percent' and curstate[unit] >= limit:
        #                     log_sent = True
        #                     getattr(self.logger, _levelToName[severity].lower())(
        #                         'Usage of resource \'{}\' in project \'{}\' is {} {} ({}/{})!'.format(mname,
        #                                                                                               'project_name',
        #                                                                                               curstate[unit],
        #                                                                                               unit,
        #                                                                                               value[keyname],
        #                                                                                               value['limit']))
        #                 if unit == 'units_left' and curstate[unit] <= limit:
        #                     log_sent = True
        #                     getattr(self.logger, _levelToName[severity].lower())(
        #                         'There is only {} units of resource \'{}\' left in project \'{}\' ({}/{})!'.format(
        #                             curstate[unit], mname, 'project_name', value[keyname], value['limit']))
        #         if log_sent:
        #             break
        #
        # exit(0)

        internal_services = {svc['type']: [ep['url'] for ep in svc['endpoints'] if ep['interface'] == 'internal'][0]
                             for svc in self.conn.service_catalog}

        for project in self.conn.identity.projects(domain_id='default'):
            # pprint(project)

            header = {'X-AUTH-TOKEN': self.conn.auth_token, 'Content-Type': 'application/json'}
            current_metrics = {}

            # Compute quotas
            r = requests.get('{}/os-quota-sets/{}/detail'.format(internal_services['compute'], project.id),
                             headers=header, verify=False)
            current_metrics.update(r.json()['quota_set'])

            # Storage quotas
            r = requests.get('{}/os-quota-sets/{}?usage=True'.format(internal_services['volumev3'], project.id),
                             headers=header, verify=False)
            current_metrics.update(r.json()['quota_set'])

            # Network quotas
            r = requests.get('{}/v2.0/quotas/{}/details.json'.format(internal_services['network'], project.id),
                             headers=header, verify=False)
            current_metrics.update(r.json()['quota'])

            for qname, qdef in quotas_json.items():
                value = current_metrics[qname]
                keyname = 'used' if 'used' in value else 'in_use'

                if value['limit'] != -1:
                    curstate = dict()
                    curstate['percent'] = round((float(value[keyname]) / float(value['limit'])) * 100, 1)
                    curstate['units_left'] = int(value['limit']) - (value[keyname])

                    sorted_qdef_severities = sorted([_nameToLevel[i] for i in qdef.keys()], reverse=True)
                    log_sent = False
                    for severity in sorted_qdef_severities:
                        for unit in quotas_json[qname][_levelToName[severity]]:
                            limit = qdef[_levelToName[severity]].get(unit)
                            if limit and not log_sent:
                                if unit == 'percent' and curstate[unit] >= limit:
                                    log_sent = True
                                    getattr(self.logger, _levelToName[severity].lower())(
                                        'Usage of resource \'{}\' in project \'{}\' is {} {} ({}/{})!'.format(
                                            qname, project.name, curstate[unit], unit,  value[keyname], value['limit']))
                                if unit == 'units_left' and curstate[unit] <= limit:
                                    log_sent = True
                                    getattr(self.logger, _levelToName[severity].lower())(
                                        'There is only {} units of resource \'{}\' left '
                                        'in project \'{}\' ({}/{})!'.format(
                                            curstate[unit], qname, project.name, value[keyname], value['limit']))


def main():

    functions = ['check_endpoints', 'check_agents', 'check_quotas', 'check_compute_services']
    
    parser = argparse.ArgumentParser()
    parser.add_argument('function', help='PyOSTMon function to call {}'.format(functions))
    args = parser.parse_args()

    if args.function in functions and hasattr(PyOSTMon, args.function):
        return getattr(PyOSTMon(), args.function)()
    else:
        parser.print_help()
        exit(1)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        exit(0)
