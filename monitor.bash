#!/bin/bash

[ "x$DEBUG" == "xtrue" ] && set -x

CURL_OPTIONS='-g --insecure -s '
OPENSTACK_OPTIONS="--insecure"
NETWORK_ENDPOINT_PORT="9696"

[ ! -s $OPENRC_FILE ] && echo "ERROR: Cannot load openstack environment file $OPENRC_FILE" && exit 1
[ "x$RSYSLOG_SERVER" == "x" ] && echo "ERROR: Environment variable \$RSYSLOG_SERVER is not set" && exit 1

source $OPENRC_FILE

function log() {
        logger --server $RSYSLOG_SERVER -P $RSYSLOG_PORT -p $RSYSLOG_FACILITY  --tcp -t "ostmon" "${1}"
}

while true;
do

TOKEN="`openstack $OPENSTACK_OPTIONS token issue -f value -c id`"
[ $? -gt 0 ] && ERRMSG="ERROR: Cannot issue openstack token." && log $ERRMSG && echo $ERRMSG && exit 1

openstack $OPENSTACK_OPTIONS endpoint list|awk '{print $14}' | grep "http:" | sort | uniq | while read URL
  do
        URL=`echo $URL|awk -F/ '{print $1 "//" $3}'`
        curl $CURL_OPTIONS -H "X-Auth-Token: $TOKEN" $URL &>/dev/null
        if [ $? -eq 0 ]
        then
                #echo "OK: Endpoint $URL is accessible."
                #logger --server $RSYSLOG_SERVER -P $RSYSLOG_PORT -p $RSYSLOG_FACILITY  --tcp "OK: Endpoint $URL is accessible."
                log "INFO: Endpoint $URL is accessible."
                URL_PORT=`echo $URL | sed -r 's/http.*:([0-9]+).*/\1/'`
                if [ "x$URL_PORT" == "x$NETWORK_ENDPOINT_PORT" ]; then
                        curl $CURL_OPTIONS --write-out "" -H "X-Auth-Token: $TOKEN" $URL/v2.0/agents | jq -r '.agents[] | select(.alive == false) | "ERROR: \(.agent_type) (id: \(.id)) on host \(.host) is down."' | while read MSG; do log "$MSG"; done
                fi

        else
                #echo "FAILED: Endpoint $URL is not accessible." 1>&2
                #logger --server $RSYSLOG_SERVER -P $RSYSLOG_PORT -p $RSYSLOG_FACILITY  --tcp "FAILED: Endpoint $URL is not accessible."
                log "CRITICAL: Endpoint $URL is not accessible."
        fi
  done

#  openstack project list --domain Default -f value -c Name | sort | while read PROJECT
#  do
#    
#  done

  sleep $SLEEP

done

