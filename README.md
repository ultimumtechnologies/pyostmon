PyOSTMon (Python Openstack Monitoring) - monitor Openstack via its API .

### Checks

The script has four parts/checks which can be run separately and with different periods. There periods can be adjusted via environment parameters as shown below. They are run by cron.

1.  endpoints - it gets all available endpoints from current Openstack installation and check the availability of their URLs.
2.  compute services - it gets status of all enabled compute services (nova-compute, nova-scheduler, nova-conductor, nova-consoleauth)
3.  agents - it gets status of all enabled network agents (neutron-l3-agent, neutron-dhcp-agent, neutron-openvswitch-agent, neutron-metadata-agent)
4.  quotas - it gets all quotas (compute, storage, network), meaning their limit, current use and compares them against defined thresholds (configured through pyostmon-quotas.json)

### Prerequisities

For script to work we need two prerequisities to be configured:

1.  Credentials to Openstack API, i.e. rc file placed on safe place on the host which runs the container
2.  Metrics threshold configuration file in JSON format placed there as well

### Parameters

There are many configurable environment variables through which we can adjust monitoring. Most of them are not needed to be changed since the defaults is set properly to be able to run in PROD environment. Their name should be self explanatory, however, here is the list with default values and a little description:

Environment variables
```
SERVER_HOSTNAME ""
This name is visible in log messages so it is easier to identify the producer when multiple monitoring containers are running. 
Note: when not specified during launch time, container ID is used.
 
KEEPALIVE "YES"
Define whether it should emit alive messages. 
E.g. when there is no keepalive message received for some amount of time an alert could be triggered.
 
HOME /root
 
OPENRC_FILE "/etc/kolla/admin-openrc.sh"
This is default path where script expects Openstack API credentials. 
It can be changed but the respective volume must be adjusted as well. 
Openstack account with admin right should be provided for proper function of all checks. 
Can be downloaded from Openstack Dashboard.
 
RSYSLOG_SERVER "syslog.example.net"
Remote syslog receiver. By default aimed at production one.
 
RSYSLOG_PORT "514"
Port the remote syslog receiver listens on.
 
RSYSLOG_FACILITY "local1.info"
Not used at the moment.
 
CRON_CHECK_COMPUTE_SERVICES="* * * * *"
CRON_CHECK_ENDPOINTS="* * * * *"
CRON_CHECK_AGENTS="*/5 * * * *"
CRON_CHECK_QUOTAS="0 * * * *"
Crontab frequencies definition for each type of check.
 
QUOTAS_JSON_FILE "/etc/kolla/pyostmon-quotas.json"
Definition metrics and their respective thresholds that are being watched.
```

### Deployment

Container build procedure
```
# docker build -t docker.registry:80/kolla/debian-binary-pyostmon .
```

Container run procedure
```
# cp admin-openrc.sh /etc/kolla/
# vim /etc/kolla/pyostmon-quotas.json
# docker run -d --name pyostmon -v /etc/kolla:/etc/kolla:ro -e "SERVER_HOSTNAME=`hostname`" -e "RSYSLOG_SERVER=syslog.example.net" docker.registry:80/kolla/debian-binary-pyostmon:latest
```

### Messages

Message samples emited by the script:
```
# pyostmon.py check_endpoints
1 2020-03-11T14:20:54.226298 56eeddc58fe1 PyOSTMon - - [msgInfo severity="INFO" hostname="56eeddc58fe1" app="PyOSTMon"] INFO: PyOSTMon alive on 56eeddc58fe1
1 2020-03-11T14:20:54.226298 56eeddc58fe1 PyOSTMon - - [msgInfo severity="CRITICAL" hostname="56eeddc58fe1" app="PyOSTMon"] CRITICAL: Barbican Key Management Service internal endpoint http://1.2.3.4:9311 is not accessible
 
# pyostmon.py check_compute_services
1 2020-03-11T14:21:38.955008 56eeddc58fe1 PyOSTMon - - [msgInfo severity="CRITICAL" hostname="56eeddc58fe1" app="PyOSTMon"] CRITICAL: Service nova-scheduler on host controller1 is down
 
# pyostmon.py check_agents
1 2020-03-11T14:22:03.190366 56eeddc58fe1 PyOSTMon - - [msgInfo severity="ERROR" hostname="56eeddc58fe1" app="PyOSTMon"] ERROR: Open vSwitch agent (id: 07eacb8e-3fbc-4961-abaf-fbbdbdefb875) on host compute5 is down
 
# pyostmon.py check_quotas
1 2020-03-11T14:22:18.021109 56eeddc58fe1 PyOSTMon - - [msgInfo severity="WARNING" hostname="56eeddc58fe1" app="PyOSTMon"] WARNING: There is only 4 units of resource 'volumes' left in project 'project_name' (16/20)
1 2020-03-11T14:22:18.021109 56eeddc58fe1 PyOSTMon - - [msgInfo severity="WARNING" hostname="56eeddc58fe1" app="PyOSTMon"] WARNING: Usage of resource 'ram' in project 'project_name' is 90.1 percent (135168/150000)
1 2020-03-11T14:22:18.021109 56eeddc58fe1 PyOSTMon - - [msgInfo severity="CRITICAL" hostname="56eeddc58fe1" app="PyOSTMon"] CRITICAL: Usage of resource 'gigabytes' in project 'project_name' is 96.0 percent (672/700)
```
